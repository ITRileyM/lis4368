
# LIS4368 - Web App Development

## Riley Moure

### Project #2 Requirements:

*Three Parts:*

1. Have display customers show customer data
2. Enable customer data to be added, updated, and deleted
3. Chapter Questions (Chs 16-17)

#### README.md file should include the following items:

* Screenshots of Valid User Form Entry and Passed Validation
* Screenshots of Data, Modify Form, Data modified, and Delete Warning
* Screenshot of Associated Database Changes (Select, Insert, Update, Delete)

#### Assignment Screenshots:

| *Screenshot of Valid User Form Entry*: | *Screenshot of Passed Validation*: |
| :--------------------------------- | ---------------------------------: |
| ![User Entry](img/valid.png "Checks indicating valid responses") | ![Passed Validation](img/passed.png "Takes you to thanks.jsp") |


***Screenshot of Data Display***
![Customer data](img/display.png "customer info")


| *Screenshot of Modify Form*: | *Screenshot of Modified data*: |
| :--------------------------------- | ---------------------------------: |
| ![modify.jsp](img/modify.png "lets you update info") | ![Passed Validation](img/modified.png "Takes you to thanks.jsp") |


***Screenshot of Delete Warning***
![Customer data](img/warning.png "customer info")


***Screenshot of Associated Database Changes (Select, Insert, Update, Delete)***
![Unchanged](img/original.png "select table")
![Inserted](img/insert.png "insert to table")
![Updated](img/update.png "update table")
![Deleted](img/delete.png "delete from table")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")
