
# LIS4368

## Riley Moure

### Assignment #5 Requirements:

*Sub-Heading:*

1. Update MySQL customer table
2. Complete Skill Sets 13-15
3. Chapter Questions (Chs 13-15)

#### README.md file should include the following items:

* Screenshots of Pre and Post valid user form entry
* Screenshots of MySQL customer table entry
* Screenshots of Skill Sets 13-15

#### Assignment Screenshots:

| *Screenshot of Pre-valid user form entry*: | *Screenshot of Post-valid user form entry*: |
| :--------------------------------- | ---------------------------------: |
| ![Pre-valid Entry](img/pre-val.png "Checks indicating valid responses") | ![Post-valid Entry](img/post-val.png "X indicating invalid responses") |

| *Screenshot of SS13: NumberSwap.java*: | *Screenshot of SS14: LargestThreeNumbers.java*: |
| :--------------------------------- | ---------------------------------: |
| ![Swap Numbers](img/swap.png "Java code") | ![Largest of Three Numbers](img/largest.png "writes into a file") |

***Screenshot of SS15: SimpleCalculatorVoidMethods.java***:
![Calculator](img/calculator.jpg "Java code")

***Screenshot of MySQL customer table***:
![Calculator](img/normal.png "Java code")

***Screenshot of updated MySQL customer table***:
![Calculator](img/updated.png "Java code")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")

