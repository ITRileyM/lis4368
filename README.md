> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 - Advanced Web Applications

## Riley Moure

### LIS4368 Requirements:

*Course Work Links:*

#### Assignments
1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repository
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install newer version of MySQL Workbench
    - Write a "Hello-world" Java Servlet
    - Write a Database Servlet
    - Compile Servlet files

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create and forward engineer an ERD
    - Provide screenshots of ERD
    - Provide links to .sql and .mwb
    - Provide screenshoots of a3 index.jsp

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Implement server-side validation
    - Provide screenshots of Failed and Passed Validation
    - Provide screenshots of Skill Sets 10-12  

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create data directory inside lis4368/WEB-INF/classes/crud
    - Create java files inside data directory called ConnectionPool, CustomerDB, and DBUtil
    - Provide screenshots of pre-, post-valid user form entry, as well as MySQL customer table entry
    - Provide screenshots of Skill Sets 13-15

#### Projects
1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Add Form Validation Plugin
    - Provide Regular Expressions
    - Provide screenshots of Passed and Failed Validation
    - Provide screenshots of p1 index.jsp

2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Complete JSP/Servlets web application using MVC framework and provide CRUD functionality
    - Provide screenshots of Valid User Form Entry and Passed Validation
    - Provide screenshots of Data, Modify Form, Data modified, and Delete Warning
    - Provide screenshots of Associated Database Changes (Select, Insert, Update, Delete)

