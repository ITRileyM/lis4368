
# LIS4368

## Riley Moure

### Assignment #4 Requirements:

*Sub-Heading:*

1. Implement server-side validation
2. Test server-side validation
3. Complete Skill Sets 10-12
4. Chapter Questions (Chs 11-12)

#### README.md file should include the following items:

* Screenshots of Failed and Passed Validation
* Screenshots of Skill Sets 10-12


#### Assignment Screenshots:

| *Screenshot of Passed Validation*: | *Screenshot of Failed Validation*: |
| :--------------------------------- | ---------------------------------: |
| ![Passed Validation](img/success.png "Checks indicating valid responses") | ![Failed Validation](img/failed.png "X indicating invalid responses") |

| *Screenshot of SS10: CountCharacters.java*: | *Screenshot of SS11: FileWriteReadCountWords.java*: |
| :--------------------------------- | ---------------------------------: |
| ![Counts Characters](img/CountCharacters.png "Java code") | ![File WRC words](img/WRCWords.png "writes into a file") |

| *Screenshot of SS12: Ascii.java*: |  *Screenshot of SS12: Ascii.java part 2*:|
| :--------------------------------- | ---------------------------------: |
| ![Ascii values](img/Ascii.png "Java code") | ![Ascii values](img/Ascii2.png "Java code") |

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")
