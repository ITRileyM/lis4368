> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 - Advanced Web Applications

## Riley Moure

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1 - 4)

#### README.md file should include the following items:

* Screenshot of running **java Hello** (#1 above);
* Screenshot of running http://localhost:9999 (#2 above, Step #4(b) in tutorial);
* Link to **local lis4368 web app**: http://localhost:9999/lis4368/
* git commands with short descriptions;
* Bitbucket repo links: 
    - a) this assignment, and 
    - b) the completed tutorial repo above (**bitbucketstationlocation**).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Git commands w/short descriptions:

1. git init - Creates an empty Git repository or reinitialize an existing one
2. git status - Shows the working tree status
3. git add - Adds file contents to the index
4. git commit - Records changes to the repository
5. git push - Updates remote refs along with associated objects
6. git pull - Fetches from and integrate with another repository or a local branch
7. git bisect - Uses binary search to find the commit that introduced a bug

#### Assignment Screenshots:

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javahello.png)


*Screenshot of running http://localhost:9999 - Tomcat*:

![Android Studio Installation Screenshot](img/Tomcat.png)


*Link to **local lis4368 web app**:*
[Web app](http://localhost:9999/lis4368/)

![lis4368 A1](img/A1.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*A1 Link - Link to current Assignment 1:*
[A1 Link](https://bitbucket.org/ITRileyM/lis4368/src/573ebb35aa8e32e87c7b0ac9ff965747bb9081af/a1/README.md "Assignment 1")
