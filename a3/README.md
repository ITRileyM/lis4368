
# LIS4368 - Web App Development

## Riley Moure

### Assignment #3 Requirements:

*Three Parts:*

1. Create ERD (Entity Relationship Diagram)
2. Populate each table with 10 records
3. chapter Questions (Chs 7-8)

#### README.md file should include the following items:

* Screenshots of ERD that links to the image
* Screenshot of a3 index.jsp
* Links to a3.sql and a3.mwb

#### Assignment Screenshot and Links:

*Screenshot A3 ERD*:

![A3 ERD](docs/a3ERD.png "ERD based upon A3 Requirements")

*Screenshots of ERD tables*:

![Petstore table](docs/petstore.png "table based upon A3 Requirements")

![Pet table](docs/pet.png "table based upon A3 Requirements")

![Customer table](docs/customer.png "table based upon A3 Requirements")

*Screenshot of A3 index.jsp*:

![A3 page](docs/a3index.png "index based upon A3 Requirements")

*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb "A3 ERD in .mwb format")

[A3 SQL File](docs/a3.sql "A3 SQL Script")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")
