<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Sat, 01-02-21, 19:40:00 Eastern Standard Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Riley Moure">
	<link rel="shortcut icon" type="image/ico" href="docs/favicon.ico">

	<title>LIS4368 - Assignment1</title>

	<%@ include file="/css/include_css.jsp" %>		
	
</head>
<body>

<!-- display application path -->
<% //= request.getContextPath()%>
	
<!-- can also find path like this...<a href="${pageContext.request.contextPath}${'/a5/index.jsp'}">A5</a> -->

	<%@ include file="/global/nav.jsp" %>	

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<%@ include file="global/header.jsp" %>
					</div>

					<b>Petstore Database (Entity Relationship Diagram):</b><br />
					<img src="docs/a3ERD.png" class="img-responsive center-block" alt="A3 ERD" />
					<br /> <br />
					
					<b>Petstore Database Tables:</b><br />
					<img src="docs/petstore.png" class="img-responsive center-block" alt="A3 ERD" /><br>
					<img src="docs/pet.png" class="img-responsive center-block" alt="A3 ERD" /><br>
					<img src="docs/customer.png" class="img-responsive center-block" alt="A3 ERD" /><br>

					<br /> <br />
					<b>MySQL Workbench and SQL Files:</b><br />
					<a href="docs/a3.mwb" style="color:#FFFF00">Petstore MySQL Workbench File</a>
				<br />
					<a href="docs/a3.sql" style="color:#FFFF00">Petstore SQL File</a>
					<br /> <br />				

	<%@ include file="/global/footer.jsp" %>

	</div> <!-- end starter-template -->
 </div> <!-- end container -->

 	<%@ include file="/js/include_js.jsp" %>		
 
</body>
</html>
