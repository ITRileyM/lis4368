
# LIS4368 - Web App Development

## Riley Moure

### Project #1 Requirements:

*Three Parts:*

1. Add Form Validation Plugin
2. Test Form Validation Plugin
3. Chapter Questions (Chs 9-10)

#### README.md file should include the following items:

* 8 Regular Expressions To Know
* Screenshots of Passed and Failed Validation
* Screenshot of p1 index.jsp


##### 8 Regular Expressions To Know:

1. *Matching a Username:* /^[a-z0-9_-]{3,16}$/
2. *Matching a Password:* /^[a-z0-9_-]{6,18}$/
3. *Matching a Hex Value:* /^#?([a-f0-9]{6}|[a-f0-9]{3})$/
4. *Matching a Slug:* /^[a-z0-9-]+$/
5. *Matching an Email:* /^([a-z0-9_\.-]+)@([\da-z\.-]+)\.([a-z\.]{2,6})$/
6. *Matching a URL:* /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/
7. *Matching an IP Address:* /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
8. *Matching an HTML Tag:* /^<([a-z]+)([^<]+)*(?:>(.*)<\/\1>|\s+\/>)$/

#### Assignment Screenshots:

| *Screenshot of Passed Validation*: | *Screenshot of Failed Validation*: |
| :--------------------------------- | ---------------------------------: |
| ![Passed Validation](img/success.png "Checks indicating valid responses") | ![Failed Validation](img/failed.png "X indicating invalid responses") |

***Screenshot of P1 index.jsp***

![P1 page](img/home.png "index based upon P1 Requirements")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")
