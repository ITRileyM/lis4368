> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368 Advanced Web Applications

## Riley Moure

### Assignment #2 Requirements:

    1. Install newer version of MySQL Workbench
    2. Write a "Hello-world" Java Servlet
    3. Write a Database Servlet
    4. Compile Servlet files

#### README.md file should include the following items:

    * Screenshot of http://localhost:9999/hello
    * Screenshot of http://localhost:9999/lis4368/a2/index.jsp
    * Screenshot of http://localhost:9999/hello/index.html
    * Screenshot of http://localhost:9999/hello/sayhello
    * Screenshot of http://localhost:9999/hello/sayhi
    * Screenshot of http://localhost:9999/hello/querybook.html
    * Screenshot of the query results

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

***Screenshot of http://localhost:9999/hello***:

![HTML files in hello](img/files.png)

***Screenshot of http://localhost:9999/hello/index.html***:

![Index file in hello](img/index.png)

*Screenshot of http://localhost:9999/hello/sayhello*: | *Screenshot of http://localhost:9999/hello/sayhi*:
--- | --- | ---
![Welcomes to page](img/helloservlet.png) | ![Welcomes back to page](img/hiservlet.png)

*Screenshot of http://localhost:9999/hello/querybook.html*: | *Screenshot of the query results*:
--- | --- | ---
![Show options to click](img/queryservlet.png) | ![Shows results](img/results.png)

***Screenshot of http://localhost:9999/lis4368/a2/index.jsp***:

![AMPPS Installation Screenshot](img/A2.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://ITRileyM@bitbucket.org/ITRileyM/bitbucketstationlocations.git "Bitbucket Station Locations")

*Link to lis4368 - Assignments & Projects:*
[lis4368 Link](https://bitbucket.org/ITRileyM/lis4368/src/master/ "Main Page")
